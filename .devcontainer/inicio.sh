
docker network inspect "$containerWorkspaceFolderBasename"_runtime >/dev/null 2>&1 || docker network create --driver bridge "$containerWorkspaceFolderBasename"_runtime
CONTAINER_ID=$(docker ps -q -f label=vsch.local.repository.folder="$containerWorkspaceFolderBasename")
if [ ! -z "$CONTAINER_ID" ]
then
    docker network connect "$containerWorkspaceFolderBasename"_runtime $CONTAINER_ID
else 
    echo no se encuentra el contenedor con el enviroment.name $containerWorkspaceFolderBasename
fi

cd $containerWorkspaceFolder
git restore . 
git clean -f

# Ya se instala en package.json
npm install -g @angular/cli
npm install -g yarn 
npm install -g vite 

mkdir -p /workspaces/code

keytool -import -alias public_cert -keystore ${JAVA_HOME}/lib/security/cacerts -file $containerWorkspaceFolder/.devcontainer/resources/public_cert.der <<EOF
changeit
yes

EOF

# cp $containerWorkspaceFolder/.devcontainer/resources/settings.template.xml  $containerWorkspaceFolder/.devcontainer/resources/settings.xml
# mvn -v --settings $containerWorkspaceFolder/.devcontainer/resources/settings.xml --global-settings $containerWorkspaceFolder/.devcontainer/resources/settings.xml

exit 0
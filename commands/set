#!/usr/bin/env bash
set -e
. "$SDG_WORKDIR/commands/common"

cli_help_set() {
  echo "
Command: set

Usage: 
  set - ajusta todos los parámetros que no estén definidos de: 
      - svn, 
      - git_repo 
      - working_domain (cambia $MVN_CONF/settings.xml)
      - Si no está defino en git el user.name y user.mail los pide
  set show - muestra todos los parámetros y sus valores si están definidos
  set entorno local/desarrollo app1 app2 app3 ... lista de aplicaciones a instalar. full (Todas las aplicaciones)
  set 'param' - ajusta el parámetro especificado. Se crea si no existe
  set help/h - muestra el usage
  "
  exit 1
}

cli_set_show () {
  [ -n "$2" ] && cli_help_set
  cli_log "showing sets & secrets"
  local svn=$(yq ".secrets" $SDG_CONF)
  echo "$svn"
}

cli_check_undefined() {
  local path=$1
  local to_check=$(yq $path $SDG_CONF)
  [[ "undefined" = "$to_check" || "null" = "$to_check" || "" = "$to_check" ]] && local rdo=1 || local rdo=0
  echo $rdo
}

cli_set_value () {
  local yq_expr=$1
  local txt=$2
  echo '   ----------------------'
  echo '   '$txt' ('"$yq_expr"'). "skip" para ignorarlo'
  sleep 0.2
  read -p '   Introduzca el valor: ' read_value
  if [ '$read_value' != 'skip' ] 
  then
    yq -i -I4 -P $yq_expr' = "'"$read_value"'"' $SDG_CONF
  fi
}

cli_set_ambito() {
  local ambito=$1

  local desc_chk=$(cli_check_undefined ".secrets."$ambito".desc")

  if [ 1 = "$desc_chk" ]
  then
      local txt='Descripción del usuario y passwork de '$ambito
      cli_set_value '.secrets.'$ambito'.desc' "$txt"
  fi

  [[ 0 = "$desc_chk" ]] && local txt=$(yq '.secrets.'$ambito'.desc' $SDG_CONF) || local txt='.secrets.'$ambito'.user'
  cli_set_value '.secrets.'$ambito'.user' "$txt"

  [[ 0 = "$desc_chk" ]] && local txt=$(yq '.secrets.'$ambito'.desc' $SDG_CONF) || local txt='.secrets.'$ambito'.pass'
  cli_set_value '.secrets.'$ambito'.pass' "$txt"

  # Configuramos los ficheros con la password working_domain (Maven)
  if [ $ambito = "working_domain" ]
  then
      local user_working_domain=$(yq ".secrets.working_domain.user" $SDG_CONF)
      local pass_working_domain=$(yq ".secrets.working_domain.pass" $SDG_CONF)

      yq -i '.settings.servers.server[].username = "'"$user_working_domain"'"' $MVN_CONF/settings.xml
      yq -i '.settings.servers.server[].password = "'"$pass_working_domain"'"' $MVN_CONF/settings.xml
  fi

  # Comprobamos la configuración de git
  local git_user_email=$(git config --get user.email)
  local git_user_name=$(git config --get user.name)
  if [[ -z "$git_user_name" || -z "$git_user_email" ]]
  then
      read -p 'Git email: ' git_user_email
      read -p 'Git user name: ' git_user_name

      git config --global user.email "$git_user_email"
      git config --global user.name "$git_user_name"
  fi
}

cli_set_change () {
  cli_log "change sets & secrets "
  local subcommand=$1
  if [ ! -n "$subcommand" ]
  then
    local user_chk=$(cli_check_undefined ".secrets.svn.user")
    local pass_chk=$(cli_check_undefined ".secrets.svn.pass" )
    [[ "1" = $user_chk || "1" = $pass_chk ]] && cli_set_ambito 'svn'

    local user_chk=$(cli_check_undefined ".secrets.git_repo.user")
    local pass_chk=$(cli_check_undefined ".secrets.git_repo.pass" )
    [[ "1" = $user_chk || "1" = $pass_chk ]] && cli_set_ambito 'git_repo'

    local user_chk=$(cli_check_undefined ".secrets.working_domain.user")
    local pass_chk=$(cli_check_undefined ".secrets.working_domain.pass" )
    [[ "1" = $user_chk || "1" = $pass_chk ]] && cli_set_ambito 'working_domain'
  else
    cli_set_ambito $subcommand
  fi
}

cli_entorno_set () {
  cli_log "Poniendo entornos "
  [[ ! -n "$2" || ! -n "$3" ]] && cli_help_set
  shift
  local new_entorno=$1

  [[ "$new_entorno" != 'local' && "$new_entorno" != 'desarrollo' ]] && cli_help_set

  shift
  local apps=()
  for i in $@; do apps+=($i) ; done

  [ ! -n "$1" ] && cli_help_fetch
  # si longitud es 1 y es full -> expandir a todas las aplicaciones
  if [[ "${#apps[@]}" -eq 1 ]] && [[ "${apps[@]}" == "full" ]]; then
    apps
    apps=("${APPS[@]}")
  fi

  for app in "${apps[@]}"; do
    cli_log "Ajustando el entorno de $app a $new_entorno"
    yq -i -I4 -P '.apps |= map (select(.app == "'$app'").entorno = "'$new_entorno'")' $SDG_CONF
  done

}


cli_set() {
  shift
  local subcommand=$1
  case "$subcommand" in
    show)
      cli_set_show $@
      ;;
    help|h)
      cli_help_set
      ;;
    entorno)
      cli_entorno_set $@
      ;;
    *)
      cli_set_change $subcommand
      ;;
  esac
}

cli_log "Setting BEGIN"
cli_set $@
cli_log "Setting END"
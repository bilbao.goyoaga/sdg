# Temas pendientes de realizar

## Modificar el sdg cli de la siguiente forma

- Poner un sdg build app -> llama a build.sh si lo hubiera de cada directorio de app
- poner un sdg deploy app -> llama a deploy.sh si lo hubiera de cada directorio de app

## Configurar el entorno con

- Añadir los pulgins de React
- Añadir tomcats nuevos
- Configurar el apache
- Asegurar que despliega todo lo nuevo
- Añadir un weblogic y configurar su despliegues

## Terminado

- mvn -v --settings /workspaces/sdg/.vscode/settings.xml --global-settings /workspaces/sdg/.vscode/settings.xml
- (*) poner un sdg start -> Levanta el servidor
- (*) poner un sdg stopt -> Levanta el servidor
- (*) Dejar el sdg con acceso global
- poner un sdg set -> pide usuario y password de svn, nexus, etc y lo guarda en sdg.yml, settings.xml
- inicio.sh pide el usuario y nombre de Git... puede llamar al 'sdg set'
- Configurar maven por defecto en el fichero
- Añadir un set app 'local' lista de apps

## notas de los servidores